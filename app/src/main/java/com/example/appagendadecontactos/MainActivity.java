package com.example.appagendadecontactos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText edtNombre, edtTelefono, edtDireccion, edtEmail;
    TextView txtNombre, txtTelefono, txtDireccion, txtEmail;
    ImageButton btEliminar, btGuardar, btCancelar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ContactoDB gbd = new ContactoDB(this,"AgendaDB", null, 1);
        edtNombre = (EditText)findViewById(R.id.txtNombre);
        edtTelefono = (EditText)findViewById(R.id.txtTelefono);
        edtDireccion = (EditText)findViewById(R.id.txtDireccion);
        edtEmail = (EditText)findViewById(R.id.txtEmail);


        Intent intent = getIntent();
        String nombre = intent.getStringExtra("nombre");
        String telefono = intent.getStringExtra("telefono");
        String direccion = intent.getStringExtra("direccion");
        String email = intent.getStringExtra("email");

        edtNombre.setText(nombre);
        edtTelefono.setText(telefono);
        edtDireccion.setText(direccion);
        edtEmail.setText(email);
    }

    public void guardarContacto(View view){
        ContactoDB gbd = new ContactoDB(this,"AgendaDB", null, 1);
        SQLiteDatabase ADB = gbd.getWritableDatabase();


        String nombre = edtNombre.getText().toString();
        String telefono = edtTelefono.getText().toString();
        String direccion = edtDireccion.getText().toString();
        String email = edtEmail.getText().toString();
        Cursor cursor = ADB.rawQuery("SELECT nombre, telefono, direccion, email from contactos WHERE nombre='" + nombre + "'", null);
        boolean contactoExistente = cursor.moveToFirst();
        cursor.close();

        ContentValues informacion = new ContentValues();
        informacion.put("nombre", nombre);
        informacion.put("telefono", telefono);
        informacion.put("direccion", direccion);
        informacion.put("email", email);



        if (contactoExistente) {

            int cant = ADB.update("contactos", informacion, "nombre=?", new String[]{nombre});
            if (cant == 1) {
                Toast.makeText(this, "SE ACTUALIZÓ CORRECTAMENTE", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "NO SE PUDO ACTUALIZAR", Toast.LENGTH_SHORT).show();
            }
        } else {

            long resultado = ADB.insert("contactos", null, informacion);
            if (resultado != -1) {
                Toast.makeText(this, "SE CREÓ CORRECTAMENTE", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "NO SE PUDO CREAR", Toast.LENGTH_SHORT).show();
            }
        }

        ADB.close();
        Intent act2 =  new Intent(this,ListaContactosActivity.class);
        edtNombre.setText("");
        edtTelefono.setText("");
        edtDireccion.setText("");
        edtEmail.setText("");
        startActivity(act2);
    }

    public void cancelarContacto(View view){
        edtNombre.setText("");
        edtTelefono.setText("");
        edtDireccion.setText("");
        edtEmail.setText("");
        Intent act2 =  new Intent(this,ListaContactosActivity.class);
        startActivity(act2);
    }

    public void eliminarContacto(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que deseas borrar este contacto?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ContactoDB gbd = new ContactoDB(MainActivity.this,"AgendaDB", null, 1);
                SQLiteDatabase ADB = gbd.getWritableDatabase();
                String nombre = edtNombre.getText().toString();
                String whereClause = "nombre=?";
                String[] whereArgs = {nombre};

                int cant = ADB.delete("contactos", whereClause, whereArgs);
                ADB.close();

                if (cant == 1){
                    Toast.makeText(MainActivity.this,"SE BORRO CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this,"NO SE BORRO CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                }
                Intent act2 =  new Intent(MainActivity.this,ListaContactosActivity.class);
                startActivity(act2);

            }
        });
        builder.setNegativeButton("Cancelar", null);

        // Mostrar el cuadro de diálogo de confirmación
        AlertDialog dialog = builder.create();
        dialog.show();

    }




}