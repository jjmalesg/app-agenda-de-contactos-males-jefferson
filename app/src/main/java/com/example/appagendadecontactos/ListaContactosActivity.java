package com.example.appagendadecontactos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ListaContactosActivity extends AppCompatActivity {


    private TableLayout tablaContactos;
    Button btBuscar;
    EditText edtBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_contactos);
        tablaContactos = (TableLayout) findViewById(R.id.tlContactos);
        edtBuscar = (EditText) findViewById(R.id.txtBuscar);
        listarContactos();

    }

    public void listarContactos() {
        tablaContactos.removeAllViews();
        ContactoDB gbd = new ContactoDB(this, "AgendaDB", null, 1);
        SQLiteDatabase ADB = gbd.getWritableDatabase();
        String nombre = edtBuscar.getText().toString();
        String aux = null;
        if (!nombre.isEmpty()) {
            aux = "SELECT nombre, telefono, direccion, email from contactos WHERE nombre='" + nombre + "'";
        } else {
            aux = "SELECT nombre, telefono, direccion, email from contactos";
        }

        Cursor cursor = ADB.rawQuery(aux, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            View layout = LayoutInflater.from(this).inflate(R.layout.item_table_layout, null, false);
            TextView txtVnombre = (TextView) layout.findViewById(R.id.tvNombre);
            TextView txtVtelefono = (TextView) layout.findViewById(R.id.tvTelefono);
            TextView txtVdireccion = (TextView) layout.findViewById(R.id.tvDireccion);
            TextView txtVemail = (TextView) layout.findViewById(R.id.tvEmail);

            txtVnombre.setText(cursor.getString(0));
            txtVtelefono.setText(cursor.getString(1));
            txtVdireccion.setText(cursor.getString(2));
            txtVemail.setText(cursor.getString(3));
            System.out.println("layout...............................................");
            tablaContactos.addView(layout);

            cursor.moveToNext();
        }

    }

    public void anadirContacto(View view) {
        Intent act1 = new Intent(this, MainActivity.class);
        startActivity(act1);
    }

    public void buscarContacto(View view) {
        listarContactos();
    }

    public void clicklistaContacto(View view) {
        TableRow contacto = (TableRow) view;
        TextView registro = (TextView) contacto.getChildAt(0);
        String nombre = registro.getText().toString();

        ContactoDB gbd = new ContactoDB(this, "AgendaDB", null, 1);
        SQLiteDatabase ADB = gbd.getWritableDatabase();
        if (!nombre.isEmpty()) {
            Cursor cursor = ADB.rawQuery("SELECT nombre, telefono, direccion, email from contactos WHERE nombre='" + nombre + "'", null);
            if (cursor.moveToFirst()) {
                String contactoNombre = cursor.getString(0);
                String telefono = cursor.getString(1);
                String direccion = cursor.getString(2);
                String email = cursor.getString(3);

                // Crear un Intent para iniciar el nuevo Activity
                Intent intent = new Intent(ListaContactosActivity.this, MainActivity.class);

                // Agregar los datos como extras en el Intent
                intent.putExtra("nombre", contactoNombre);
                intent.putExtra("telefono", telefono);
                intent.putExtra("direccion", direccion);
                intent.putExtra("email", email);

                // Iniciar el nuevo Activity
                startActivity(intent);

            } else {
                Toast.makeText(this, "No hay el Contacto", Toast.LENGTH_SHORT).show();
            }

        }

    }
}


